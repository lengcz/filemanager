<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="false"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<head>
<title>消息提醒</title>
</head>

<body>
	<%@include file="top.jsp"%>
	<hr>
	<h3>${message}</h3>
	<br>

	<%--<c:if test="${downloadUrl !=null }">
		<a href="${ downloadUrl}">下载此文件</a>
	</c:if>--%>
	<c:if test="${listFile!=null && listFile.size()>0}">
	<table border=1 style="border-collapse: collapse;">
		<tr style="background-color: #9e9e9e9c;">
			<td width="200">文件名</td>
			<td width="160">文件大小</td>
			<td width="160">上传时间</td>
			<td width="100">操作</td>
		</tr>
		<!-- 遍历Map集合 -->
		<c:forEach var="f" items="${listFile}">
			<tr>
				<td>${f.name}</td>
				<td>${f.fileLengthStr }</td>
				<td>${f.uploadTimeStr}</td>
				<td><a href="DownLoadServlet?filename=${f.fullNameBase64}"><button >下载</button></a></td>
			</tr>

		</c:forEach>
	</table>
	</c:if>


</body>
</html>