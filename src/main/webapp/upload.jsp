<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<head>
<title>文件上传</title>
</head>

<body>
	<h2>文件管理</h2>
	<a href="index.jsp">前往主页</a>
	<a href="upload.jsp">上传文件</a>
	<a href="./servlet/ListFileServlet">文件列表</a>
	<a href="javascript:history.go(0)">刷新</a>
	<hr>
	<h2>上传文件</h2>
	<form action="servlet/UploadHandleServlet"
		enctype="multipart/form-data" method="post">
		上传文件1：<input type="file" name="file1">
		<br><br>
		 上传文件2：<input type="file" name="file2"><br> <br>
		 上传文件3：<input type="file" name="file3"><br><br>
		 <input type="submit" value="提交">
	</form>
	<hr>
	注:可同时上传一个或多个文件<br>
	注:最大允许单个文件大小512MB
</body>
</html>