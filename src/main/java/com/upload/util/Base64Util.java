package com.upload.util;


import java.io.IOException;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Base64编码与转码
 * <p>
 * 类名称：Base64Util   <br>
 * 类描述：   <br>
 * 创建人：leng  <br>
 * 修改备注：   <br>
 */
public class Base64Util {

    /**
     * 编码
     *
     * @param s
     * @return
     */
    public static String getBase64(String s) {
        if (s == null) return null;
        return new BASE64Encoder().encode(s.getBytes());
    }

    /**
     * 解码
     *
     * @param s
     * @return
     */
    public static String getFromBase64(String s) {
        if (s == null) return null;
        BASE64Decoder decoder = new BASE64Decoder();

        byte[] b;
        try {
            b = decoder.decodeBuffer(s);
            return new String(b);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) {
        String s = "Test  44";
        String str = getBase64(getBase64(s));
        System.out.println(str);

        String str2 = getFromBase64(getFromBase64(str));
        System.out.println(str2);
    }

}