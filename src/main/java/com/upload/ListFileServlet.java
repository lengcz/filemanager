package com.upload;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.upload.constant.SplitConstant;
import com.upload.entity.FileInfo;
import com.upload.util.Base64Util;
import com.upload.util.DateUtils;

public class ListFileServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 201805089626295L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 获取上传文件的目录
        String uploadFilePath = this.getServletContext().getRealPath("/WEB-INF/upload");
        // 存储要下载的文件名

        List<FileInfo> listFile = new ArrayList<FileInfo>();
        listfile(new File(uploadFilePath), listFile);// File既可以代表一个文件也可以代表一个目录


        Collections.sort(listFile, new Comparator<FileInfo>() {

            public int compare(FileInfo o1, FileInfo o2) {
                // 返回值为int类型，大于0表示正序，小于0表示逆序
                return (int) (o2.getUploadTime() - o1.getUploadTime());
            }
        });

        request.setAttribute("listFile", listFile);

        double totalLength = 0D;// 文件总大小
        for (FileInfo f : listFile) {
            totalLength = totalLength + f.getFileLength();
        }

        DecimalFormat df = new DecimalFormat("0.00");

        request.setAttribute("fileCount", listFile.size());
        request.setAttribute("totalLength", df.format(totalLength / (1024 * 1024)));
        request.getRequestDispatcher("/listfile.jsp").forward(request, response);
    }

    public void listfile(File file, List<FileInfo> listFile) throws UnsupportedEncodingException {
        // 如果file代表的不是一个文件，而是一个目录
        if (!file.isFile()) {
            // 列出该目录下的所有文件和目录
            File files[] = file.listFiles();
            if (null != files && files.length > 0) {
                // 遍历files[]数组
                for (File f : files) {
                    // 递归
                    listfile(f, listFile);
                }
            }
        } else {
            /**
             * 处理文件名，上传后的文件是以uuid_文件名的形式去重新命名的，去除文件名的uuid_部分
             * file.getName().indexOf("_")检索字符串中第一次出现"_"字符的位置，如果文件名类似于：
             * 9349249849-88343-8344_阿_凡_达.avi
             * 那么file.getName().substring(file.getName().indexOf("_")+1)
             * 处理之后就可以得到阿_凡_达.avi部分
             */
            String realName = file.getName().substring(
                    file.getName().lastIndexOf(SplitConstant.SPLIT_SYMBOL) + SplitConstant.SPLIT_SYMBOL.length());
            // file.getName()得到的是文件的原始名称，这个名称是唯一的，因此可以作为key，realName是处理过后的名称，有可能会重复

            // String[] fileInfos =
            // file.getName().split(SplitConstant.SPLIT_SYMBOL);

            FileInfo f = new FileInfo();
            f.setName(realName);
            f.setFullName(file.getName());
            String fullName = URLEncoder.encode(Base64Util.getBase64(file.getName()), "UTF-8");
            f.setFullNameBase64(fullName);
            try {
                double filelength = file.length();
                f.setFileLength(filelength);
                DecimalFormat df = new DecimalFormat("0.00");
                if (filelength / (1024 * 1024) > 1) {
                    f.setFileLengthStr(df.format(filelength / (1024 * 1024)) + " MB");
                } else if (filelength / (1024) > 1) {
                    f.setFileLengthStr(df.format(filelength / (1024)) + " KB");
                } else {
                    f.setFileLengthStr(filelength + " Byte");
                }

                // long uploadTime = Long.parseLong(fileInfos[fileInfos.length -
                // 2]);
                long uploadTime = file.lastModified();
                f.setUploadTime(uploadTime);
                f.setUploadTimeStr(DateUtils.getDateString(uploadTime));
            } catch (Exception e) {
                e.printStackTrace();
            }

            listFile.add(f);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}